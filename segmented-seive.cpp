#include <bits/stdc++.h>
#define MAX 1000000
using namespace std;

/*

Performs seive operation in the given range ( start, end )
It also requires prime numbers below this range
Change MAX value to chage the range in each iteration
#define MAX 1000000

*/
void seive(int start, int end, vector<int> &primes){
	vector<int> v(MAX, 1);
	for(int i = 0; i < primes.size(); i++)
		for(int j = start/primes[i]; j <= end/primes[i]; j++)
			if( primes[i]*j >= start)
				v[ (primes[i]*j) - start ] = 0;
	for(int i = start; i < end; i++){
		if(v[ i - start ] && i > 1){
			primes.push_back(i);
			for(int p = i; p <= end/i; p++){
				v[ (i*p)- start ] = 0;
			}
		}
	}
}

/*
Input: range - It should be in multiple of 10^5
Output : prime numbers
*/
vector<int> segmented_seive(int range){
	vector<int> primes;
	int s = 0, e = MAX;
	while(e <= range){
		seive(s, e, primes);
		//cout<<primes.size()<<endl;
		s = e;
		e += MAX;
	}
	return primes;
}