void printMultiplication(vector<int> &v){
	while(!v.empty())
		cout<<v.back();
	cout<<endl;
}

/*
Note: LSB at Index 0, MSB at Index v.size()-1
*/
vector<int> multiply(string &sa, string &sb, int base){
	
	/*Creating input*/ 
	vector<int> a, b;
	for(int i=sa.length() - 1; i>=0; i--)
		a.pb(sa[i] - '0');
	for(int i=sb.length()-1; i>=0; i--)
		b.pb(sb[i] - '0');
	
	/*Numbers are right to left*/
	vector<int> v(size(a) + size(b));
	for(int i=0; i < size(a); i++){
		for(int j=0; j < size(b); j++){
			if(a[i] == 0)
				continue;
			v[i+j] += b[j]*a[i];
		}
	}
	
	/*For Binary*/
	for(int i=0; i<size(a) + size(b)-1; i++){
		v[i+1] += v[i]/base;
		v[i] = v[i]%base;
	}

	while(size(v) >1 && v.back() == 0)
		v.pop_back();
	
	return v;
}
