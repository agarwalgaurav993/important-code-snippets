#include <bits/stdc++.h>
using namespace std;
#define MAX 1000000
typedef pair<int, int> iPair;
int tw;
vector<int> Prims(vector<vector<iPair > > &graph, int S){
	/*Takes in Graph and returns Parent Vector*/
	vector<int> visited(graph.size());
	vector<int> dist(graph.size(), INT_MAX);
	vector<int> parent(graph.size());
	priority_queue<iPair, vector<iPair > , greater<iPair > > pq;
	pq.push(make_pair(0, S));
	parent[S] = S;
	//int tw = 0;
	while(!pq.empty()){
		int i = pq.top().second;
		int w = pq.top().first;
		pq.pop();
		if(!visited[i]){
			visited[i] = 1;
			tw += w;
			for(int k=0; k<graph[i].size(); k++){
				int j = graph[i][k].first;
				if(!visited[j]){
					if(dist[j] > graph[i][k].second){
						dist[j] = graph[i][k].second;
						parent[j] = i;
					}
					pq.push(make_pair(graph[i][k].second, graph[i][k].first));
				}
			}
		}
	}
	/*Parent Vector Starts from 0/1 depending on Node Number 0-(N-1)/1-N/ */
	return parent;
}

int Prims_MSTLength(vector<vector<iPair > > &graph, int S){
	/*Takes in Graph and returns MST weight Only*/
	vector<int> visited(MAX);
	priority_queue<iPair, vector<iPair > , greater<iPair > > pq;
	pq.push(make_pair(0, S));
	int tw = 0;
	while(!pq.empty()){
		int i = pq.top().second;
		int w = pq.top().first;
		pq.pop();
		if(!visited[i]){
			visited[i] = 1;
			tw += w;
			for(int k=0; k<graph[i].size(); k++)
				if(!visited[graph[i][k].first])
					pq.push(make_pair(graph[i][k].second, graph[i][k].first));
		}
	}
	return tw;
}

int main(){
	int N, M;
	cin>>N>>M;
	vector<vector<iPair > > graph(N+1);
	for(int i=0; i<M; i++){
		int x, y, w;
		cin>>x>>y>>w;
		graph[x].push_back(make_pair(y, w));
		graph[y].push_back(make_pair(x, w));
	}
	int S;
	cin>>S;
	vector<int> p = Prims(graph, S);
	for(int i=1; i<p.size(); i++)
		cout<<p[i]<<" ";
	cout<<endl;
	return 0;
}