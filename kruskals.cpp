#include <bits/stdc++.h>
using namespace std;
/*Kruskals; to find the MST based on SET-UNION*/
struct Edge{
	int x, y, w;
	Edge(int a,int b,int c) : x(a), y(b), w(c) {}
};
bool cmp(Edge e1, Edge e2){
	if(e1.w == e2.w)
		return e1.x + e1.y < e2.x + e2.y;
	return e1.w < e2.w;
}
int find(int x, vector<int> &parent){
	if(x != parent[x])
		parent[x] = find(parent[x], parent );
	return parent[x];
}
void unioun(int x, int y, vector<int> &parent){
	int px = find(x, parent);
	int py = find(y, parent);
	if(px != py)
		parent[px] = py;
}
/*Returns MST weight*/
int Kruskals(vector<Edge> &v, int N){
	vector<int> parent;
	for(int i=0; i<N; i++)
		parent.push_back(i);
	sort(v.begin(), v.end(), cmp);
	int a, b;
	int path = 0;
	for(int i=0; i<v.size(); i++){
		a = v[i].x-1;
		b = v[i].y-1;
		a = find(a, parent);
		b = find(b, parent);
		if(a != b){
			path += v[i].w;
			parent[a] = b;
		}
	}
	return path;
}

int main(){
	int N, M;
	cin>>N>>M;
	vector<Edge> v;
	Edge e(0,0,0);
	for(int i=0; i<M; i++){
		cin>>e.x>>e.y>>e.w;
		v.push_back(e);
	}
	cout<<Kruskals(v, N)<<endl;
	return 0;
}