void explore(vector<int> &nodes, vector<int> &visited, queue<int> &q, int mydist, vector<int> &dist){
	for(int i=0; i<nodes.size();i++){
		if(!visited[nodes[i]]){
			visited[nodes[i]] = 1;
			dist[nodes[i]] = mydist + 1;
			q.push(nodes[i]);
		}
	}
}
vector<int> shortestReach(vector<vector<int> > &graph, int S){
	vector<int> dist(graph.size(), INT_MAX);
	vector<int> visited(graph.size());
	queue<int> q;
	q.push(S);
	dist[S] = 0;
	while(!q.empty()){
		int N = q.front();
		q.pop();
		explore(graph[N], visited, q, dist[N], dist);
	}
	return dist;
}
