#include <bits/stdc++.h>
#define INF INT_MAX
using namespace std;
typedef pair<int, int> iPair;
int mx = INF;
int mflow =0;
void print(vector<vector<int> > &digraph){
	for(int i=1; i<digraph.size(); i++){
		for(int j=1; j<digraph[i].size(); j++)
			cout<<digraph[i][j]<<" ";
		cout<<endl;
	}
}
bool augmentedPath(vector<vector<int> > &digraph, int parent, int cmx, int destination, vector<int> &visited){
	if(parent == destination){
		mx = cmx;
		mflow += cmx;
		return true;
	}
	visited[parent] = 1;
	for(int i=1; i<digraph[parent].size(); i++){
		if(visited[i] == 0 && digraph[parent][i] != 0 && augmentedPath(digraph, i, min(cmx, digraph[parent][i]), destination, visited)){
			digraph[i][parent] +=mx;
			digraph[parent][i] -= mx;
			//visited[parent] = 0;
			return true;
		}
	}
	//visited[parent] = 0;
	return false;
}
int maxFlow(vector<vector<int > > &digraph, int S, int T){
	mflow = 0;
	vector<int> visited(digraph.size());
	while(augmentedPath(digraph, S, INF, T, visited)){
		fill(visited.begin(), visited.end(), 0);
		//cout<<endl;
		//print(digraph);
	}
	return mflow;
}

int main(){
	int N, M;
	cin>>N>>M;
	vector<vector<int> > digraph(N+1, vector<int>(N+1));
	for(int i=0; i<M; i++){
		int s, e, w;
		cin>>s>>e>>w;
		digraph[s][e] = w;
	}
	print(digraph);
	int S, T;
	cin>>S>>T;
	cout<<maxFlow(digraph, S, T)<<endl;
	return 0;
}