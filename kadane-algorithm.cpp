#define size(s) (int)s.size()

int KadaneAlgorithm(vector<int> &v){
	int mx = INT_MIN;
	int csum = 0;
	for(int i=0; i<size(v); i++){
		if(csum < 0 )
			csum = 0;
		csum += v[i];
		mx = max(csum, mx);
	}
	return mx;
}
