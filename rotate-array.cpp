#include <bits/stdc++.h>
#define pb push_back
#define ll long long
#define mp make_pair
using namespace std;


int gcd(int x, int y){
	if(y == 0)
		return x;
	return gcd(y, x%y);
}
/*Left to Right rotation*/
/*O(N) solution with O(1) space*/
void rotateArray(vector<int> &v, int k){
	int N = (int)v.size();
	int l = gcd(k, N);
	cout<<l<<endl;
	for(int i=0, j; i<l; i++){
		int temp = v[i];
		int p = i;
		for(j=(i+k)%N; j!=i; j= (j+k)%N){
			v[p] = v[j];
			p = j;
		}
		v[p] = temp;
	}
}


/*
void print(vector<int> &v){
	for(int i=0; i<v.size(); i++)
		cout<<v[i]<<" ";
	cout<<endl;
}
int main(){
	int N, K;
	cin>>N;
	vector<int> v(N);
	for(int i=0; i<N; i++)
		cin>>v[i];
	cin>>K;
	rotateArray(v, K);
	return 0;
}*/