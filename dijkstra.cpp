#include <bits/stdc++.h>
using namespace std;
/*Takes in Adjecency Matrix of a Graph and a Single Source as input and returns the distance to every other node.*/
/*
*INPUT: Adjeceny MATRIX of Graph
		Single Source S
*OUTPUT:
		vector<int> of distances from S
*/
typedef pair<int, int> iPair;

vector<int> dijkstra(vector<vector<int> > &graph, int S){
	priority_queue<iPair, vector<iPair >, greater<iPair > > pq;
	vector<int> d(graph.size(), INT_MAX);
	d[S] = 0;
	pq.push(make_pair(0, S));
	while(!pq.empty()){
		iPair p = pq.top();
		pq.pop();
		int i = p.second;
		int w = p.first;
		for(int j=0; j<graph[i].size(); j++){
			if(graph[i][j] != INT_MAX && d[j] > d[i] + graph[i][j]){
				d[j] = d[i] + graph[i][j];
				pq.push(make_pair(d[j], j));
			}
		}
	}
	return d;
}


int main(){
	int T;
	cin>>T;
	while(T--){
		int N, M, S;
		cin>>N>>M;
		int x, y;
		vector<vector<int> > graph(N, vector<int>(N, INT_MAX));
		for(int i=0; i<M; i++){
			cin>>x>>y;
			cin>>graph[x-1][y-1];
			graph[y-1][x-1] = min(graph[y-1][x-1], graph[x-1][y-1]);
			graph[x-1][y-1] = graph[y-1][x-1];
		}
		cin>>S;
		vector<int> v = dijkstra(graph, S-1);
		for(int i=0; i<v.size(); i++)
			if(i != S-1)
				if(v[i] == INT_MAX)
					cout<<"-1 ";
				else
					cout<<v[i]<<" ";
		cout<<endl;
	}

	return 0;
}