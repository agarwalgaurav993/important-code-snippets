int msp(string &a, string &b){
	int N = a.length();
	int M = b.length();
	vector<vector<int> > v(N + 1, vector<int> (M + 1));
	for(int i=0; i<=N; i++)
		for(int j=0; j<=M ; j++){
			if(i == 0|| j == 0)
				v[i][j] = 0;
			else if(a[i-1] == b[j-1]){
				v[i][j] = 1 + v[i-1][j-1];
			}
			else{
				v[i][j] = max(v[i-1][j], v[i][j-1]);
			}
		}
	
	
	//findMidChars(a, v[N][N]/2 + 1);
	return v[N][N];
}

